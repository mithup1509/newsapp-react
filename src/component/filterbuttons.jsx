import React from 'react';



function Filterbuttons(props) {
 
    return ( 
        <div className='filter' >
            <div >
                <label >Search</label>
                <select name="searching" id="searchnews" onChange={props.searchon}>
                    
                    <option value="by">Author</option>
                    <option value="title">Title</option>
                </select>
            </div>
            <div >
                <label >sortby</label>
                <select name="sorting" id="sortnews" onChange={props.sorton}>
                    
                    <option value="score">popularity</option>
                    <option value="time">Date</option>
                </select>
            </div>

            <div >
                <label >for</label>
                <select name="date" id="searchlatest" onChange={props.searchfor}>
                    <option value="">All</option>
                    <option value={`${(new Date(new Date().getTime() - (24 * 60 * 60 * 1000))).getTime()}`}>last24h</option>
                    <option value= {`${(new Date(new Date() - 7 * 24 * 60 * 60 * 1000)).getTime()}`}  >LastWeek</option>
                    <option value={`${(new Date(new Date())).setMonth(new Date(new Date()).getMonth()-1)}`} >Lastmonth</option>
                    <option value={`${(new Date(new Date())).setFullYear(new Date(new Date()).getFullYear()-1)}`} >Lastyear</option>
                </select>
            </div>

        </div>
     );
}

export default Filterbuttons;