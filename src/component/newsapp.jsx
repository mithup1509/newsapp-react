import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import Searchbar from "./searchbar";
import Newsdisplay from "./newsdisplay";
import Filterbuttons from "./filterbuttons";
import './newsapp.css'
function Newsapp() {
  const [news, setNews] = useState([]);
  const [searchon, setSearchon] = useState("by");
  const [sorton,setSorton]=useState("score");
  const [display,setDisplay]=useState(["content loading"]);
  const [searchforvalue,setSearchforvalue]=useState("");



  useEffect(() => {
    
    fetch("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        let datas = data.slice(-100);
      
        const promiseArr = datas.map((element) => {
          
          return fetch(
            `https://hacker-news.firebaseio.com/v0/item/${element}.json?print=pretty`
          )
            .then((res) => res.json())
            .then((data1) => {
              
              setNews((prevState) => [...prevState, data1]);
              setDisplay((prevState) => [...prevState, data1]);
              
            });
          
        });
        return Promise.all(promiseArr);
      })

      .catch((err) => Promise.reject(err));
    
  }, []);



  
  const startSearching = (e) => {
    let copydata=[...news]; 
  copydata .filter((element)=>{
    return ((new Date(element.time*1000)).getTime())>=searchforvalue;
  })
  // setDisplay(copydata);




    let copynews = copydata;
    
    copynews.sort((a,b)=> {
      
      return (b[sorton] - a[sorton])
    });
    
  
    let array = copynews.filter((element) => {
     
      return element[searchon.toLowerCase()]
        .toLowerCase()
        .includes(e.target.value.toLowerCase());
    });

    setDisplay(array);

  };
  const searchonvalue = (e) => {
    setSearchon(e.target.value);
    
  };


const sortonvalue=(e)=>{
  
  setSorton(e.target.value)
  
}

const searchfor=(e)=>{
  
  setSearchforvalue(e.target.value);
}

  return (
    <div className="fullcontainer">
    
      <Searchbar handleSearch={startSearching} />
      <Filterbuttons searchon={searchonvalue} sorton={sortonvalue} searchfor={searchfor} />
      <Newsdisplay news={   display.filter((element)=>{
  return (((new Date(element.time*1000)).getTime())>=searchforvalue)
}).sort((a,b)=> {
    
    return (b[sorton] - a[sorton])
  })
  } />
    </div>
  );
}

export default Newsapp;
