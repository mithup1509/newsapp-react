import React from 'react';

function Eachnews({news}) {
    

    return ( 
        <div className='newscontainer' >
            {news.map((element)=>{
                let newdate=new Date(element.time*1000);
                
                return <div key={element.id} className='eachNews' >
                <div>
                <h4>{element.title}</h4>
            </div>
            <a href={""+element.url}>{element.title.slice(0,25)}....</a>
            <div className='news-metadata'>
                <p>{element.descendants} comments</p>
                <p className='slash'>|</p>
                <p>{element.by}</p>
                <p className='slash'>|</p>
                <p>{newdate.toDateString()}</p>
                <p className='slash'>|</p>
                <p>score {element.score}</p>
                <p className='slash'>|</p>
                <p>{element.type}</p>
                
            </div>
            </div>
            })}
            
        </div>
    );
}

export default Eachnews;