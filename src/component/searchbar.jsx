
import React from 'react';



function Searchbar({handleSearch}) {
    return (
        <div className='search-bar' >
            
            <h3>Search News</h3>
            <div className='search'  >
    
                <input type="text" onChange={handleSearch} placeholder='Search stories by title ,url or author' className='search-input'/>
            </div>
            <div className='setting' >
            
            <h3>Settings</h3>
            </div>
            
        </div>
      );
}

export default Searchbar;